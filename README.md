# Resource Version Hash

A tool to get current resource version hash of Kirara Fantasia asset server.

## What is Resource Version Hash

Resource Version Hash is a strategy to prevent downloading assets when Kirara Fantasia server is in maintain. The hash code separates different versions of assets and only when the server is ready the hash will be published by Kirara Fantasia server. One who wants the latest assets and databases must use the latest Resource Version Hash.

## Use as an executable

```
go get gitlab.com/kirafan/resource-version-hash/krrrvh
hash=$(krrrvh)
```

## Use as a Go package

```
import rvh "gitlab.com/kirafan/resource-version-hash"


func main() {
	hash = rvh.Get()
}
```
